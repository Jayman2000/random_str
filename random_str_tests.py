# random_str - Python module for generating random strings
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.
from typing import Final, Optional
from unittest import main, TestCase

import random_str


ITERATIONS: Final = 100
NEGATIVE_AMOUNT_WARNING: Final = "No warning for negative amount."
TO_CHOOSE_FROM_WARNING: Final = "No warning for empty to_choose_from."


class RandomCharacter(TestCase):
    def helper(self, to_choose_from: Optional[str] = None) -> None:
        result = random_str.random_character(to_choose_from)
        self.assertEqual(len(result), 1)
        if to_choose_from is not None:
            self.assertIn(result, to_choose_from)

    def test_0_args(self) -> None:
        for unused_variable in range(ITERATIONS):
            self.helper()

    def test_1_arg(self) -> None:
        TO_CHOOSE_FROM_ERROR: Final = 'No error when to_choose_from is empty.'
        with self.assertRaises(ValueError, msg=TO_CHOOSE_FROM_ERROR):
            self.helper("")

        for length in range(1, ITERATIONS + 1):
            for unused_variable in range(ITERATIONS):
                to_choose_from = random_str.random_str(length)
                self.helper(to_choose_from)


class RandomCharacters(TestCase):
    def helper(
                self,
                amount: int,
                to_choose_from: Optional[str] = None
            ) -> None:

        total = 0
        for character in random_str.random_characters(amount, to_choose_from):
            self.assertEqual(len(character), 1)
            if to_choose_from is not None:
                self.assertIn(character, to_choose_from)
            total += 1

        if amount < 0:
            self.assertEqual(total, 0)
        else:
            self.assertEqual(total, amount)

    def test_1_arg(self) -> None:
        # amount < 0
        for amount in range(-ITERATIONS, 0):
            for unused_variable in range(ITERATIONS):
                with self.assertWarnsRegex(
                        UserWarning,
                        "amount",
                        msg=NEGATIVE_AMOUNT_WARNING):
                    self.helper(amount)
        # amount >= 0
        for amount in range(ITERATIONS):
            for unused_variable in range(ITERATIONS):
                self.helper(amount)

    def test_2_args(self) -> None:
        # amount can be < 0, but it should be >= 0.

        # **amount < 0, so to_choose_from shouldn't be "", but it can be.**
        to_choose_from = ""
        for amount in range(-ITERATIONS, 0):
            # TODO
            # I think that the following code should work, but it doesn't.
            # with self.assertWarnsRegex(
            #         UserWarning,
            #         "amount",
            #         msg=NEGATIVE_AMOUNT_WARNING):
            #     with self.assertWarnsRegex(
            #             UserWarning,
            #             "to_choose_from",
            #             msg=TO_CHOOSE_FROM_MSG):
            #         self.helper(amount, to_choose_from)

            # Workarround:
            with self.assertWarns(UserWarning) as context_manager:
                self.helper(amount, to_choose_from)
            warnings = context_manager.warnings

            self.assertEqual(len(warnings), 2)
            self.assertIn(
                "amount",
                str(context_manager.warnings[0].message),
                msg=NEGATIVE_AMOUNT_WARNING)
            self.assertIn(
                "to_choose_from",
                str(context_manager.warnings[1].message),
                msg=TO_CHOOSE_FROM_WARNING)

        # to_choose_from is not "".
        for amount in range(-ITERATIONS, 0):
            for length in range(1, ITERATIONS + 1):
                to_choose_from = random_str.random_str(length)

                with self.assertWarnsRegex(
                        UserWarning,
                        "amount",
                        msg=NEGATIVE_AMOUNT_WARNING):
                    self.helper(amount, to_choose_from)

        # **amount == 0, so to_choose_from shouldn't be "", but it can be.**
        # to_choose_from is "".
        with self.assertWarnsRegex(
                UserWarning,
                "to_choose_from",
                msg=TO_CHOOSE_FROM_WARNING):
            self.helper(0, "")

        # to_choose_from is not "".
        for length in range(1, ITERATIONS + 1):
            to_choose_from = random_str.random_str(length)
            self.helper(0, to_choose_from)

        # **amount > 0, so to_choose_from can't be "".**
        # to_choose_from is "".
        for amount in range(1, ITERATIONS + 1):
            with self.assertRaises(ValueError):
                self.helper(amount, "")

        # to_choose_from is not "".
        for amount in range(1, ITERATIONS + 1):
            for length in range(1, ITERATIONS + 1):
                to_choose_from = random_str.random_str(length)
                self.helper(amount, to_choose_from)


class RandomStr(TestCase):
    def helper(
                self,
                length: int,
                to_choose_from: Optional[str] = None
            ) -> None:
        result = random_str.random_str(length, to_choose_from)

        if length < 0:
            self.assertEqual(len(result), 0)
        else:
            self.assertEqual(len(result), length)

        if to_choose_from is not None:
            for character in result:
                self.assertIn(character, to_choose_from)

    def test_1_arg(self) -> None:
        # length < 0
        for length in range(-ITERATIONS, 0):
            with self.assertWarnsRegex(
                    UserWarning,
                    "amount",
                    msg=NEGATIVE_AMOUNT_WARNING):
                self.helper(length)

        # length >= 0
        for length in range(ITERATIONS):
            for unused_variable in range(ITERATIONS):
                self.helper(length)

    def test_2_args(self) -> None:
        # length can be < 0, but it should be >= 0.

        # **length < 0, so to_choose_from shouldn't be "", but it can be.**
        # to_choose_from is "".
        to_choose_from = ""
        for length in range(-ITERATIONS, 0):
            for unused_variable in range(ITERATIONS):
                # TODO
                # Same workaround from RandomCharacters.test_2_args().
                with self.assertWarns(UserWarning) as context_manager:
                    self.helper(length, to_choose_from)
                warnings = context_manager.warnings

                self.assertEqual(len(warnings), 2)
                self.assertIn(
                    "amount",
                    str(context_manager.warnings[0].message),
                    msg=NEGATIVE_AMOUNT_WARNING)
                self.assertIn(
                    "to_choose_from",
                    str(context_manager.warnings[1].message),
                    msg=TO_CHOOSE_FROM_WARNING)

        # to_choose_from is not "".
        for length in range(-ITERATIONS, 0):
            for to_choose_from_length in range(ITERATIONS):
                to_choose_from = random_str.random_str(to_choose_from_length)

                with self.assertWarnsRegex(
                        UserWarning,
                        "amount",
                        msg=NEGATIVE_AMOUNT_WARNING):
                    self.helper(length, to_choose_from)

        # **length == 0, so to_choose_from shouldn't be "", but it can be.**
        # to_choose_from is "".
        with self.assertWarnsRegex(
                    UserWarning,
                    "to_choose_from",
                    msg=TO_CHOOSE_FROM_WARNING):
            self.helper(0, "")

        # to_choose_from is not "".
        for to_choose_from_length in range(1, ITERATIONS + 1):
            to_choose_from = random_str.random_str(to_choose_from_length)
            self.helper(0, to_choose_from)

        # **length > 0, so to_choose_from can't be "".**
        # to_choose_from is "".
        for length in range(1, ITERATIONS + 1):
            for unused_variable in range(ITERATIONS):
                with self.assertRaises(ValueError):
                    self.helper(length, "")

        # to_choose_from is not "".
        for length in range(1, ITERATIONS + 1):
            for to_choose_from_length in range(1, ITERATIONS + 1):
                to_choose_from = random_str.random_str(to_choose_from_length)
                self.helper(length, to_choose_from)


if __name__ == "__main__":
    main()
