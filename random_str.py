# random_str - Python module for generating random strings
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.
from random import choice, randint
from typing import Final, Iterator, Optional
from warnings import warn


def random_character(to_choose_from: Optional[str] = None) -> str:
    if to_choose_from is None:
        return chr(randint(0, 0x10ffff))
    else:
        try:
            return choice(to_choose_from)
        except IndexError as old:
            NEW: Final = \
                ValueError("The argument, to_choose_from, can't be empty.")
            raise NEW from old


def random_characters(
            amount: int,
            to_choose_from: Optional[str] = None
        ) -> Iterator[str]:
    if amount <= 0:
        if amount < 0:
            MESSAGE: Final = \
                "The argument, amount, is {}. Generating zero characters..."
            warn(MESSAGE.format(amount))

        if to_choose_from is not None and len(to_choose_from) == 0:
            warn("The argument, to_choose_from, is empty.")

    for unused_variable in range(amount):
        yield random_character(to_choose_from)


def random_str(length: int, to_choose_from: Optional[str] = None) -> str:
    return "".join(random_characters(length, to_choose_from))
