#!/bin/bash

# random_str - Python module for generating random strings
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

PYTHONDEVMODE=1
function test_module() {
    file_name="$*.py"

    echo "$file_name"
    python3 -W all $file_name
    pylint -d C0114,C0115,C0116,R1705 $file_name
    pycodestyle $file_name
    mypy --strict --disallow-any-expr --tb $file_name
}

test_module random_str
test_module random_str_tests
