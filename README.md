# random_str
#### Copying
Some parts of this mod were made by other people or weren't dedicated to the public domain using CC0. Here's a list of stuff that wasn't created by me and where it appears in this repository:
  * [CC0 software notice](https://wiki.creativecommons.org/wiki/CC0_FAQ#May_I_apply_CC0_to_computer_software.3F_If_so.2C_is_there_a_recommended_implementation.3F) by [Diane Peters](https://wiki.creativecommons.org/wiki/User:CCID-diane_peters), [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
    * A modified version of this notice is at the top of every souce code file.
  * [The CC0 1.0 Universal legal code as plaintext](https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt) by [Creative Commons](https://creativecommons.org), [CC0](https://creativecommons.org/publicdomain/zero/1.0).
    * COPYING.txt

